FROM debian:11.6
WORKDIR /usr/src/app
COPY . .
RUN apt-get update -y && \
  apt-get install -y python3-pip
EXPOSE 5000
RUN pip3 install -r requirements.txt
CMD [ "python3", "app.py"]



